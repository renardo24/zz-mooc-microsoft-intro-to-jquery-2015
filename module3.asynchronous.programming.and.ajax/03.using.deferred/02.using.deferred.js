// Sample worker script

// Send ready message (again -sample implementation)
self.postMessage('READY');

// sleep for two seconds
// Simulate long-running operation
sleep(2000);

// Send completed message
self.postMessage('COMPLETED')

function sleep(millisesonds) {
    var startingTime = new Date().getTime();
    var stopTime = startingTime + millisesonds;

    while (stopTime >= new Date().getTime()) {}
}
